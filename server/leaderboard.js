// On server startup, create some players if the database is empty.
  Meteor.startup(function () {
    Players.remove({});
    if (Players.find().count() === 0) {
      var names = ["Gimli",
                   "Legolas",
                   "Aragorn"
                  ];

      for (var i = 0; i < names.length; i++)
        Players.insert({name: names[i], score: Math.floor(Random.fraction()*10)});
    }
  });
