Template.leaderboard.helpers({
    players: function () {
        return Players.find({}, {sort: {score: -1, name: 1}});
    },
    selected_name: function () {
        var player = Players.findOne(Session.get("selected_player"));
        return player && player.name;
    },
    selected_player: function() {
        var player = Players.findOne(Session.get("selected_player"));
        return player;
    }
});

Template.player.helpers({
    selected: function () {
        return Session.equals("selected_player", this._id) ? "selected" : '';
    }
});

Template.leaderboard.events({
    'click button.inc': function () {
        var selectedPlayer = Players.findOne(Session.get("selected_player"));
        Players.update(Session.get("selected_player"), {$inc: {score: 1}});
    }
});

Template.player.events({
    'click': function () {
        Session.set("selected_player", this._id);
    }
});